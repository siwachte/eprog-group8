import java.util.ArrayList;

public class node {
        //Node references
        node left;
        node right;

        int value;

        //for our DFS to store if we already visited
        boolean visited;

        //adjacency List
        ArrayList<node> adjacentNodes;

        public void addEdge(node newNeighbor) {
            adjacentNodes.add(newNeighbor);
        }
}
