import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

/***************************************************************************
****************************************************************************
***************************Solution idea by Luca Faga***********************
***************************************************************************
****************************************************************************
/*
We want to determine for each String if it is a palindrome or not
*/
public class recPalindrome {
    //static String s;
    public static void main(String[] args){

        try {
            //create Scanner
            Scanner sc = new Scanner(new File("/home/simon/SynologyDrive/ETH/20HS/EProg_TA/eprog-group8/recPalindrome/palindroms.txt"));
            //get the number of Strings in the file
            int n = sc.nextInt();
            sc.nextLine();
            //go through all Strings
            for(int i = 0; i < n; i++){
                String s = sc.next();
                //print the name of the String and if it is a palindrome of not
                System.out.println(s + ": " + isPalindrome(s));
            }
        } catch (FileNotFoundException e) {
            System.out.println("The file was not found");
        }


    }
    static boolean isPalindrome(String str) {
        //check if the string has length one, if it does we return true
        if(str.length() > 1) {
            //check is the last character and the first character are equal
            if(str.charAt(0) == str.charAt(str.length()-1)) {
                //recursive call where we cut the first and last character from our String
                if(isPalindrome(str.substring(1, str.length()-1))) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return true;
        }
    }
}