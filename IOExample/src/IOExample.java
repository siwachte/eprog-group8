import java.io.File;
import java.io.FileNotFoundException;
import java.util.Locale;
import java.util.Scanner;

public class IOExample {
        public static void main(String[] args) {
            Scanner sc = null;

            //don't worry about try and catch for now
            try {
                sc = new Scanner(new File("data.txt"));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            //to demonstrate failing Locale
            //works if set to Locale.ENGLISH
            sc.useLocale(Locale.GERMAN);

            //see how many lines the file has
            //ACHTUNG: This is a convention and done in most cases for the files that you have to read
            int lines = sc.nextInt();
            sc.nextLine();

            if(sc.hasNextLine()){
                String str = sc.nextLine();
                System.out.println(str);
            }
            if(sc.hasNextInt()){
                int number = sc.nextInt();
                System.out.println(number);
            }
            if(sc.hasNextDouble()){
                double d = sc.nextDouble();
                System.out.println(d);
            }
        }
}
