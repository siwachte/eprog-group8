public class Mergesort {
    
    public int[] sort(int[] arr) {

        //call the recursive split method
        split(arr, 0, arr.length-1);

        //return the sorted array
        return arr;
    }

    //this method splits the array from index left to index right into two parts
    public void split(int[] array, int left, int right) {

        //this statement will end the recursion when the size of the array is 
        if(left < right) {

            //this will calculate the middle index
            int middle = (right - left) / 2 + left;

            //we give both subarrays to the split method
            split(array, left, middle);
            split(array, middle+1, right);
            //this will actually merge the array
            merge(array, left, right, middle);
        }
    }
    //this method produces a sorted array from index left to index right
    //it assumes that the array is already sorted from left to middle and from middle+1 to right
    public void merge(int[] array, int left, int right, int middle) {

        //three counter variables
        int l = left;
        int r = middle+1;
        int currentPos = left;

        //we clone the array
        //it is important to note, that we save the array twice in memory and not just copy the reference
        int[] clone = array.clone();

        //we traverse thtough both parts of the array and copy the smaller element to the new array
        while(l < middle+1 && r < right+1) {

            if(clone[r] < clone[l]){
                array[currentPos] = clone[r];
                r++;

            } else {
                array[currentPos] = clone[l];
                l++;
            }
            currentPos++;
        }
        
        //this will just copy the remaining elements to the new array
        while(l < middle+1){
            array[currentPos] = clone[l];
            l++;
            currentPos++;
        }
        while(r < right+1){
            array[currentPos] = clone[r];
            r++;
            currentPos++;
        }

    }
}
