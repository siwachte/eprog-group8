
public class LinkedList {
	Node first;

	public LinkedList() {
		first = null;
	}

	public void addLast(int value) {
		if (first == null) {
			Node neu = new Node(value, null);
			first = neu;
			return;
		} 
			Node current = first;

			while (current.next != null) {
				current = current.next;
			}

			Node neu = new Node(value, null);
			current.next = neu;
	}
	
	
	public int remove(int index) {
		if (first == null) {
			System.out.println("List is empty");
			System.exit(-1);
		}
		
		if (index == 0) {
			int value = first.value;
			first = first.next;
			return value;
		}
		
		
		Node current = first;
		for (int i = 0; i < index-1; i++) {
			current = current.next;
			
			if (current.next == null) {
				System.out.println("Index out of Bound");
				System.exit(-1);
			}
		}
		int value = current.next.value;
		current.next = current.next.next;
		
		return value;
	}

}

class Node {
	int value;
	Node next;

	public Node(int v, Node n) {
		value = v;
		next = n;
	}
}
