import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class GenerateSolutionFile {

	public static void main(String[] args) throws IOException {
		
		Scanner sc = new Scanner(new File("numbers.txt"));		
		OutputStream out = new FileOutputStream(new File("sol_factors.txt"));
		Writer fout = new OutputStreamWriter(out);
		
		
		int m = sc.nextInt();
		fout.write(Integer.toString(m) + "\n");
		fout.flush();
		
		for (int i = 0; i < m; i++) {
			int n = sc.nextInt();
			List<Integer> prims = getprimfactors(n);
			
			for (int j = 0; j < prims.size(); j++) {
				fout.write(Integer.toString(prims.get(j)) + " ");
			}
			fout.write("\n");
			fout.flush();
		}	
	}
	
	public static List<Integer> getprimfactors(int n){
		ArrayList<Integer> primfactors = new ArrayList<Integer>();
		
		if (n < 0) {
			primfactors.add(-1);
		} else if (n > 0) {
			primfactors.add(1);
		} else {
			return primfactors;
		}
		
		n = Math.abs(n);
		
		while (isNotPrime(n)) {
			
			int border = (int) Math.sqrt(n);
			
			for (int i = 2; i <= border; i++) {
				if (n % i == 0) {
					primfactors.add(i);
					n = n / i;
					i = border + 1;
				}
			}
		}
		
		primfactors.add(n);
		
		return primfactors;
	}




static boolean isNotPrime(int k) {
	
	for (int i = 2; i <= Math.sqrt(k); i++) {
		if (k % i == 0) {
			return true;
		}
	}
	return false;
}

}
