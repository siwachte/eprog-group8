import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PrimeZerlegung {
    
    public void getPrimeZerlegung() {
        
        try {
            Scanner sc = new Scanner(new File("numbers.txt"));
            OutputStream out = new FileOutputStream(new File("primefactors.txt"));
            Writer writes = new OutputStreamWriter(out);

            int m = sc.nextInt();
            String mm = Integer.toString(m);
            writes.write(mm + "\n");
            
            for (int i = 0; i < m; i++) {
                int n = sc.nextInt();
                List<Integer> prims = getprimfactors(n);
                
                for (int j = 0; j < prims.size(); j++) {
                    writes.write(Integer.toString(prims.get(j)) + " ");
                }
                writes.write("\n");
                writes.flush();
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //implement this    
	public static List<Integer> getprimfactors(int n){
		ArrayList<Integer> primfactors = new ArrayList<Integer>();
		
		
		return primfactors;
	}

}
