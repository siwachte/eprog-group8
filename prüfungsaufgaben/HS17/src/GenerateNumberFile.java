import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

public class GenerateNumberFile {

    public static void main(String[] args) {

        int MAX_NUMBER = 10000;
        try {
            OutputStream outStream = new FileOutputStream("numbers.txt");

            Writer fout = new OutputStreamWriter(outStream);
            String max = Integer.toString(MAX_NUMBER-1);
            fout.write(max + "\n");
            fout.flush();

            for (int i = 1; i < MAX_NUMBER; i++) {
                String str = Integer.toString(i);
                fout.write(str + "\n");
                fout.flush();
            }
            fout.close();

        } catch (FileNotFoundException e) {
            System.out.println("File was not found");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}