import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import org.junit.Test;

public class PrimeZerlegungTest {

    @Test
    public void test(){
        PrimeZerlegung pz = new PrimeZerlegung();
        pz.getPrimeZerlegung();

        try {
            Scanner toTest = new Scanner(new File("primefactors.txt"));
            Scanner solution = new Scanner(new File("sol_factors.txt"));

            int n = toTest.nextInt();
            int m = solution.nextInt();

            for(int i = 0; i < n; i++){
                assertTrue(solution.nextInt() == toTest.nextInt());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
